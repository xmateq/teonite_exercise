from rest_framework.generics import ListAPIView
from scraper.utils import get_words, get_authors, get_authors_words
from scraper.models import Stats, Author, SingleAuthorWords
from scraper.serializers import StatsSerializer, AuthorSerializer, SingleAuthorWordsSerializer


class StatsView(ListAPIView):
    get_words()
    queryset = Stats.objects.all()
    serializer_class = StatsSerializer


class AuthorsView(ListAPIView):
    get_authors()
    queryset = Author.objects.all()
    serializer_class = AuthorSerializer


class SingleAuthorsWordsView(ListAPIView):
    get_authors_words()
    serializer_class = SingleAuthorWordsSerializer

    def get_queryset(self):
        return SingleAuthorWords.objects.filter(nickname=self.kwargs['nickname'])
