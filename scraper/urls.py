from django.urls import path
from scraper.views import StatsView, AuthorsView, SingleAuthorsWordsView

urlpatterns = [
    path('stats/', StatsView.as_view()),
    path('authors/', AuthorsView.as_view()),
    path('stats/<str:nickname>', SingleAuthorsWordsView.as_view()),
]
