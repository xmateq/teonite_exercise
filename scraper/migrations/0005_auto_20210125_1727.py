# Generated by Django 3.1.3 on 2021-01-25 16:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('scraper', '0004_auto_20210125_1719'),
    ]

    operations = [
        migrations.AlterField(
            model_name='author',
            name='names_and_nicknames',
            field=models.JSONField(),
        ),
        migrations.AlterField(
            model_name='stats',
            name='words',
            field=models.JSONField(),
        ),
    ]
