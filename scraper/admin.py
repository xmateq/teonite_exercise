from django.contrib import admin
from scraper.models import Stats, Author, SingleAuthorWords


@admin.register(Stats)
class StatsModelAdmin(admin.ModelAdmin):
    pass


@admin.register(Author)
class AuthorModelAdmin(admin.ModelAdmin):
    pass


@admin.register(SingleAuthorWords)
class SingleAuthorWordsModelAdmin(admin.ModelAdmin):
    pass
