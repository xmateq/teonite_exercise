import requests
from bs4 import BeautifulSoup
from collections import Counter
from string import punctuation
from scraper.models import Stats, Author, SingleAuthorWords

URL = 'https://teonite.com/blog/'
BLOG_PAGES = 8


def get_article_links():
    print("Data scraping started...")
    print("Getting article links...")
    all_links = []
    for page in range(1, BLOG_PAGES + 1):
        response = requests.get(URL + '{}'.format(f'page/{page}'))
        if response.status_code != 200:
            response = requests.get(URL)

        soup = BeautifulSoup(response.content, 'html.parser')

        articles = soup.find(class_='container blog-wrapper')

        single_article = articles.find_all(class_='post-title')

        a_tag = [tag.find('a') for tag in single_article]
        single_page_links = [link['href'] for link in a_tag]
        all_links.append(single_page_links)
    return all_links


LINKS = get_article_links()
AUTHORS = []


def get_data_from_all_articles_on_page(page):
    ignore = ['the', 'or', 'of', 'to', 'and', 'a', 'is', 'you', 'in', 'we', 'it', 'that', 'are', 'this', 'for', 'on',
              'i', 'be', 'as', 'not', 'have', 'was', 'your', 'an', 'can', 'our', 'with', 'will', 'if', 'what', '',
              'also', 'which', 'by', 'from', 'at', 'us', 'so', 'but', 'they', 'there']
    total_words_from_single_page = []
    for article in range(len(LINKS[page])):
        response = requests.get(URL + '{}'.format(LINKS[page][article]))
        soup = BeautifulSoup(response.content, 'html.parser')
        article_content = soup.find(class_='post-content')
        text = (''.join(s.findAll(text=True)) for s in article_content.find_all('p'))
        words_counter = Counter((word.rstrip(punctuation).lower() for y in text for word in y.split()))
        author = soup.find(class_='author-name').text
        AUTHORS.append(author)

        for word in ignore:
            if word in words_counter:
                del words_counter[word]

        total_words_from_single_page.append(words_counter)

    final_sum_one_page = Counter()
    for one_article in total_words_from_single_page:
        final_sum_one_page.update(**one_article)
    return final_sum_one_page


def sum_of_words_of_all_pages():
    print("Collecting data...")
    full_data = Counter()
    for page in range(len(LINKS)):
        full_data.update(get_data_from_all_articles_on_page(page=page))
    return dict(full_data.most_common(10))


words_dictionary = sum_of_words_of_all_pages()


def authors_without_duplicates():
    authors = AUTHORS
    authors_final = []
    authors_final_dict = {}

    for author in authors:
        if author not in authors_final:
            authors_final.append(author)
    for name in authors_final:
        authors_final_dict.update({name.replace(' ', '').lower(): name})
    return authors_final_dict


authors_dictionary = authors_without_duplicates()


def get_words_per_author():
    print("Connecting words to authors...")
    ignore = ['the', 'or', 'of', 'to', 'and', 'a', 'is', 'you', 'in', 'we', 'it', 'that', 'are', 'this', 'for', 'on',
              'i', 'be', 'as', 'not', 'have', 'was', 'your', 'an', 'can', 'our', 'with', 'will', 'if', 'what', '',
              'also', 'which', 'by', 'from', 'at', 'us', 'so', 'but', 'they', 'there', '-']
    all_article_links = []
    for page in LINKS:
        for link in page:
            all_article_links.append(link)
    article_texts = [BeautifulSoup(requests.get(URL + '{}'.format(url)).content, 'html.parser') for url in all_article_links]

    authors_names_list = [value for key, value in authors_dictionary.items()]
    authors_words = dict.fromkeys(authors_names_list)

    for key, value in authors_words.items():
        words = []
        for text in article_texts:
            text_author = text.find('span', class_='author-name').text
            if key == text_author:
                words_p_tag = text.find('div', class_='post-content').text
                words.append(words_p_tag.split())
        all_words = [word for paragraphs in words for word in paragraphs]
        authors_words[key] = all_words

    words_per_author = {}
    for key, value in authors_words.items():
        words_counter = Counter()
        for one_word in value:
            words_counter[one_word] = value.count(one_word)
        for word in ignore:
            if word in words_counter:
                del words_counter[word]
        words_per_author[key] = words_counter.most_common(10)

    for key, value in words_per_author.items():
        final_words = {}
        for count in value:
            final_words[count[0]] = count[1]
        words_per_author[key] = final_words
    return words_per_author


authors_words = get_words_per_author()


def get_words():
    Stats.objects.get_or_create(words=words_dictionary)


def get_authors():
    Author.objects.get_or_create(names_and_nicknames=authors_dictionary)


def get_authors_words():
    for key, value in authors_words.items():
        SingleAuthorWords.objects.get_or_create(word_count=value, name=key, nickname=key.lower().replace(' ', ''))
