from rest_framework import serializers
from scraper.models import Stats, Author, SingleAuthorWords


class StatsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Stats
        fields = ('words', )


class AuthorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Author
        fields = ('names_and_nicknames', )


class SingleAuthorWordsSerializer(serializers.ModelSerializer):
    class Meta:
        model = SingleAuthorWords
        fields = ('word_count', 'name', 'nickname',)
