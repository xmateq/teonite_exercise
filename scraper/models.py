from django.db import models


class Stats(models.Model):
    words = models.JSONField()


class Author(models.Model):
    names_and_nicknames = models.JSONField()


class SingleAuthorWords(models.Model):
    name = models.CharField(max_length=50)
    nickname = models.CharField(max_length=50)
    word_count = models.JSONField()

    def __str__(self):
        return self.name
