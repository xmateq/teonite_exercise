# Teonite recruitement exercise (backend)

Web scraper for collecting data from Teonite blog and adding it to PostgreSQL database.

## Installation

#### 1. Clone repository
#### 2. Navigate to root directory

#### 3. For installation use package manager - pip

```pip install -r requirements.txt```

### About

API has three available endpoints:

##### http://127.0.0.1:8000/stats - number of words overall
##### http://127.0.0.1:8000/authors - authors names with id's
##### http://127.0.0.1:8000/stats/kamilchudy - number of words used by given author
